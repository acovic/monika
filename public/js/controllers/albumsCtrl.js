(function () {

    var albumsCtrl = function (Restangular) {
        var $ctrl = this;
        $ctrl.albums = [];

        Restangular.all('albums').getList()
            .then(function (value) {
                console.log(value);
                $ctrl.albums = value;
            })
            .catch(function (reason) {
                console.log('Error...', reason);
            });

        console.log($ctrl.albums);
    };

    angular.module('galleryApp').controller('albumsCtrl', albumsCtrl);

}());