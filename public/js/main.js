(function () {

    var app = angular.module('galleryApp', [
        'ui.router',
        'restangular'
    ]);

    /**
     * UI Router Configuration
     */
    app.config(function ($stateProvider) {

        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: './js/partial-home.html'
            });
        $stateProvider
            .state('albums', {
                url: '/albums',
                templateUrl: './js/partial-albums.html',
                controller: 'albumsCtrl',
                controllerAs: '$ctrl'
            });


    });
    /**
     * Restangular Configuration
     */
    app.config(function (RestangularProvider) {
        RestangularProvider.setBaseUrl('https://jsonplaceholder.typicode.com/');
    });

}());